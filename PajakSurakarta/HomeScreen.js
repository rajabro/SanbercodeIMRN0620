import React, {Component} from 'react';
import {  Platform, View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { AntDesign } from '@expo/vector-icons';
import layananData from './layananData.json';
// import YouTube from 'react-native-youtube';

export default class App extends React.Component{
    // constructor(props) {
    //     super(props);
    //     this.state = {
    //         isReady: false,
    //         status: "",
    //         quality: "",
    //         error: ""
    //     };
    // }
    render() {
        return (
            <View style={styles.container}>
                <View style= {styles.navBar}>
                    <Image source={require('./assets/logo_pajak_biru.png')} style={{width:44, height:59}}/>
                    <View style= {styles.textLogo}>
                        <Text style= {styles.textLogo}>PAJAK</Text>
                        <Text style= {styles.textLogo}>SURAKARTA</Text>
                    </View>
                    <View style={styles.rightNav}>
                        <TouchableOpacity>
                            <AntDesign name= "login" style={styles.rightNav} size={25}/>
                            <Text style={styles.textRightNav}>Sign In</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{marginTop: 10}}>
                    <Text style={styles.title}>
                        Selamat Datang
                    </Text>
                    <Text style={styles.title}>
                        di Aplikasi Layanan
                    </Text>
                    <Text style={styles.title}>
                        KPP Pratama Surakarta
                    </Text>
                </View>
                {/* <View>
                    <YouTube
                        apiKey="AIzaSyBTVCj0dV0k74iVxXfcy-XcMf_4T_8kqSw"
                        videoId="begK0OE6ac0" // The YouTube video ID
                        play={true} // control playback of video with true/false
                        fullscreen={false} // video should play in fullscreen or inline
                        loop={false} // control whether the video should loop when ended
                        onReady={e => this.setState({ isReady: true })}
                        onChangeState={e => this.setState({ status: e.state })}
                        onChangeQuality={e => this.setState({ quality: e.quality })}
                        onError={e => this.setState({ error: e.error })}
                        style={styles.youtube}
                    />
                </View> */}
                <FlatList
                    data={layananData.items}
                    keyExtractor={(item, index) => index.toString()}
                    showsVerticalScrollIndicator={false}
                    renderItem={({ item }) => {
                            return (
                                <View style={{flex: 1, flexDirection: 'column'}}>
                                    <TouchableOpacity>
                                    <View style={styles.layananContainer}>
                                        <View style={styles.icon}>
                                            <Icon name= {item.iconName} 
                                                size= {60} color= "white" />
                                        </View>
                                        <View style={styles.layananItem}>
                                            <Text style={
                                                {
                                                    fontWeight: 'bold', 
                                                    color:'white', 
                                                    fontSize:25, 
                                                    textAlign:'left',
                                                }}>{item.layanan}</Text>
                                            <Text style={
                                                {
                                                    fontWeight: 'bold', 
                                                    color: 'white', 
                                                    fontSize: 13, 
                                                    textAlign:'left',
                                                    flexWrap: 'wrap'
                                                }}>{item.deskripsi}</Text>
                                        </View>
                                        <View style={{flex: 2, backgroundColor:'#F2C64D', padding: 5, alignItems: 'center', justifyContent: 'center'}}>
                                            <Icon name="chevron-double-right" size= {60} color= "white" />
                                        </View>
                                    </View>
                                    </TouchableOpacity>
                                </View>
                            )
                }}
                />
                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="home" size={25} color = "red"/>
                        <Text style={styles.tabTitle}>Home</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Icon name="account-circle" size={25} />
                        <Text style={styles.tabTitle}>About Us</Text>
                    </TouchableOpacity>
                    </View>
            </View>
        )
    }
};

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#213067',
    },
    navBar: {
        height: 60,
        backgroundColor: 'white',
        elevation: 3,
        flexDirection: 'row',
        alignItems: 'flex-start',
        justifyContent: 'space-between',
        marginTop: 30
    },
    textLogo: {
        flexDirection: 'column',
        justifyContent: 'space-around',
        fontFamily: 'Roboto',
        fontWeight: 'bold',
        fontSize: 22,
        marginLeft: -60,
        alignItems: 'flex-start'
    },
    title: {
        fontFamily: 'Roboto',
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        fontSize: 24,
        textAlign: 'center',
    },
    youtube: {
        alignSelf: 'stretch',
        height: 300,
    },
    rightNav:{
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center',
        padding: 5

    },
    textRightNav: {
        marginTop: -5
    },
    navItem: {
        marginLeft: 25
    },
    body:{
        flex: 1
    },
    tabBar: {
        backgroundColor: 'white',
        height: 60,
        borderTopWidth: 0.5,
        borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-around',
        marginTop: 15
    },
    tabItem: {
    alignItems: 'center',
    justifyContent: 'center',
    color: '#213067'
    },
    tabTitle: {
        fontSize: 11,
        color: '#8c8b8b',
        paddingTop: 4
    },
    layananContainer: {
        marginTop: 15,
        marginHorizontal: 20,
        backgroundColor: '#EBB623',
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        height: 95,
        width: '90%',
        shadowColor: "black",
        shadowOffset: {
	        width: 0,
	        height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,

        elevation: 5,
    },
    icon: {
        flex: 3,
        justifyContent : "center",
        alignItems: 'center',
        backgroundColor: '#F2C64D'
    },
    layananItem: {
        flex: 9,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'flex-start',
        padding: 5
    },
})