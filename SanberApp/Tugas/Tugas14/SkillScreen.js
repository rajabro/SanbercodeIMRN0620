import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  ImageBackground,
  Text,
  TextInput,
  ScrollView,
  Dimensions, FlatList
} from "react-native";
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { AntDesign } from '@expo/vector-icons';
import skillData from './skillData.json';


export default class App extends React.Component {
  // state = {
  //   skillData
  // }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logo}>
          <Image source={require('./images/logo_putih.png')} style={{width:187.5, height:51}}/>
          <Text style={styles.logoText}>PORTOFOLIO</Text>
        </View>
        <View style={styles.profile}>
          <Image source={require('./images/foto_profile.jpg')} style={styles.photoProfile}/>
          <View style={styles.profileTitle}>
            <Text style={styles.hai}>Hai,</Text>
            <Text style={styles.profileName}>Rubai Amin Jaya</Text>
          </View>
        </View>
        <View style={styles.skillTitle}>
          <Text style={styles.textSkill}>SKILL</Text>
          <View style={{backgroundColor:'#3EC6FF', height:4}}></View>
        </View>
        <View style={styles.rekapSkillContainer}>
          <Text style={styles.rekapSkill}>Library / Framework</Text>
          <Text style={styles.rekapSkill}>Bahasa Pemrograman</Text>
          <Text style={styles.rekapSkill}>Teknologi</Text>
        </View>
        {/* <View style={styles.skillContainer}>
          <View style={styles.icon}>
            <Icon name="react" size= {100} color= "#003366" />
          </View>
          <View style={styles.skillItem}>
            <Text style={{fontWeight: 'bold', color:'#003366', fontSize: 24, textAlign:'left'}}>React Native</Text>
            <Text style={{fontWeight: 'bold', color: '#3EC6FF', fontSiz: 16, textAlign:'left'}}>Library / Framework</Text>
            <Text style={{fontWeight:'bold', color: '#FFFFFF', fontSize: 48, textAlign:'right'}}>50%</Text>
          </View>
          <View style={styles.icon}>
            <AntDesign name="right" size= {80} color= "#003366" />
          </View>
        </View> */}
        <FlatList
          data={skillData.items}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          renderItem={({ item }) => {
            return (
              <View style={{flex: 1, flexDirection: 'column'}}>
              <View style={styles.rekapSkillContainer}>
              </View>
              <View style={styles.skillContainer}>
                <View style={styles.icon}>
                  <Icon name= {item.iconName} size= {100} color= "#003366" />
                </View>
                <View style={styles.skillItem}>
                  <Text style={{fontWeight: 'bold', color:'#003366', fontSize: 24, textAlign:'left'}}>{item.skillName}</Text>
                  <Text style={{fontWeight: 'bold', color: '#3EC6FF', fontSiz: 16, textAlign:'left'}}>{item.categoryName}</Text>
                  <Text style={{fontWeight:'bold', color: '#FFFFFF', fontSize: 48, textAlign:'right'}}>{item.percentageProgress}</Text>
                </View>
                <View style={styles.icon}>
                  <AntDesign name="right" size= {80} color= "#003366" />
                </View>
              </View>
              </View>
            )
          }}
        />






      </View>

    )};
  }

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    padding: 15
  },
  logo: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignItems:'flex-end',
  },
  logoText:{
    textAlign: 'right',
    fontSize:12,
    color:'#00d0ff',
    right: 15,
    fontFamily: 'Roboto',
    position: 'absolute'
  },
  profile: {
    width: 34,
    height: 34,
    borderRadius: 25,
    justifyContent: "flex-start",
    flexDirection: 'row'
  },
  photoProfile: {
    width: 34,
    height: 34,
    borderRadius: 25,
  },
  profileTitle: {
    flexDirection: "column",
    justifyContent: 'space-between',
    alignItems:'flex-start',
    marginLeft: 10
  },
  hai: {
    fontFamily: "Roboto",
    color: "#003366",
    fontSize: 12
  },
  profileName: {
    fontFamily: "Roboto",
    color: "#003366",
    fontSize: 16,
    fontWeight: 'bold'
  },
  skillTitle: {
    marginTop: 15,
    flexDirection: "column",
  },
  textSkill:{
    fontSize: 36, 
    fontFamily: 'Roboto',
    fontWeight: 'bold',
    color: '#003366',
  },
  rekapSkillContainer:{
    flexDirection: "row",
    alignItems: 'center',
    justifyContent: 'space-around',
    marginTop: 15
  },
  rekapSkill:{
    color: '#003366',
    backgroundColor: '#B4E9FF',
    borderRadius:25,
    padding : 5,
  },
  skillContainer: {
    marginTop: 15,
    padding: 10,
    backgroundColor: '#B4E9FF',
    borderRadius:25,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 129,
  },
  icon: {
    flexDirection: 'column',
    justifyContent : "center",
    alignItems: 'flex-start',
  },
  skillItem: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  // image: {
  //   width: 200,
  //   height: 213,
  //   justifyContent: "flex-end",
  // },
  // portofolio: {
  //   fontFamily: "Roboto",
  //   color: "rgba(62,198,255,1)",
  //   marginTop: 119,
  //   marginLeft: 100
  // },
  // nameTitle: {
  //   top: 167,
  //   left: 44,
  //   position: "absolute",
  //   fontFamily: "Roboto",
  //   color: "rgba(0,51,102,1)",
  //   fontSize: 16
  // },
  // skill: {
  //   position: "absolute",
  //   fontFamily: "Roboto",
  //   color: "rgba(0,51,102,1)",
  //   fontSize: 36,
  //   width: 341,
  //   height: 43,
  //   bottom: 0,
  //   left: 0
  // },
  // imageStack: {
  //   top: 0,
  //   left: 0,
  //   width: 359,
  //   height: 231,
  //   position: "absolute"
  // },
  
  
  // imageStackStack: {
  //   width: 359,
  //   height: 231
  // },
  // loremIpsum: {
  //   fontFamily: "Roboto",
  //   color: "#121212",
  //   marginLeft: 28,
  //   marginTop: 186
  // },
  // imageStackStackRow: {
  //   height: 231,
  //   flexDirection: "row",
  //   marginTop: -53,
  //   marginLeft: 16,
  //   marginRight: -110
  // },
  // group: {
  //   width: 341,
  //   height: 14,
  //   flexDirection: "row",
  //   justifyContent: "space-between",
  //   marginTop: 9,
  //   marginLeft: 16
  // },
  // textInput: {
  //   fontFamily: "Roboto",
  //   color: "#121212",
  //   width: 109,
  //   backgroundColor: "rgba(180,233,255,1)",
  //   borderRadius: 4,
  //   fontSize: 12,
  //   textAlign: "center",
  //   alignSelf: "stretch"
  // },
  // textInput2: {
  //   fontFamily: "Roboto",
  //   color: "#121212",
  //   width: 126,
  //   backgroundColor: "rgba(180,233,255,1)",
  //   borderRadius: 4,
  //   fontSize: 12,
  //   textAlign: "center",
  //   alignSelf: "stretch"
  // },
  // textInput3: {
  //   fontFamily: "Roboto",
  //   color: "#121212",
  //   width: 60,
  //   backgroundColor: "rgba(180,233,255,1)",
  //   borderRadius: 4,
  //   fontSize: 12,
  //   textAlign: "center",
  //   alignSelf: "stretch"
  // },
  // rect: {
  //   width: 341,
  //   height: 3,
  //   backgroundColor: "rgba(62,198,255,1)",
  //   marginTop: -21,
  //   marginLeft: 16
  // },
  // scrollArea: {
  //   top: 0,
  //   left: 3,
  //   width: 343,
  //   height: 129,
  //   position: "absolute",
  //   backgroundColor: "rgba(180,233,255,1)",
  //   shadowColor: "rgba(0,0,0,0.25)",
  //   shadowOffset: {
  //     width: 0,
  //     height: 3
  //   },
  //   elevation: 12,
  //   shadowOpacity: 0.25,
  //   shadowRadius: 4,
  //   borderRadius: 8
  // },
  // scrollArea_contentContainerStyle: {
  //   height: 129,
  //   width: 343
  // },
  // reactNative: {
  //   fontFamily: "Roboto",
  //   color: "rgba(0,51,102,1)",
  //   fontSize: 24
  // },
  // libraryFramework: {
  //   fontFamily: "Roboto",
  //   color: "rgba(62,198,255,1)",
  //   fontSize: 16
  // },
  // loremIpsum2: {
  //   fontFamily: "Roboto",
  //   color: "rgba(255,255,255,1)",
  //   fontSize: 48,
  //   marginTop: 7,
  //   marginLeft: 52
  // },
  // reactNativeColumn: {
  //   width: 143
  // },
  // icon2: {
  //   color: "rgba(0,51,102,1)",
  //   fontSize: 54,
  //   height: 59,
  //   width: 54,
  //   marginLeft: 49,
  //   marginTop: 19
  // },
  // reactNativeColumnRow: {
  //   height: 113,
  //   flexDirection: "row",
  //   marginTop: 10,
  //   marginLeft: 97
  // },
  // icon: {
  //   top: 7,
  //   left: 0,
  //   position: "absolute",
  //   color: "rgba(0,51,102,1)",
  //   fontSize: 94
  // },
  // scrollAreaStack: {
  //   width: 346,
  //   height: 129,
  //   marginTop: 29,
  //   marginLeft: 13
  // }
});
