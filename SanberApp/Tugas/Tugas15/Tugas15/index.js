import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator} from '@react-navigation/bottom-tabs';

import { SignIn, CreateAccount, Profile, Home} from './Screen';

const AuthStack = createStackNavigator();
const Tabs = createBottomTabNavigator();

// export default class SigninScreen extends React.Component {
export default () => (
  <NavigationContainer>
    <Tabs.Navigator>
      <Tabs.Screen name="Home" component={Home} />
      <Tabs.Screen name="Home" component={Profile} />
    </Tabs.Navigator>
    <AuthStack.Navigator>
    <AuthStack.Screen 
        name='SignIn' 
        component={SignIn} 
        option={{ title: 'Sign In' }}
      />
      <AuthStack.Screen 
        name='CreateAccount' 
        component={CreateAccount} 
        option={{ title: 'Create Account'}} 
    />
    </AuthStack.Navigator>
  </NavigationContainer>
);
// }

