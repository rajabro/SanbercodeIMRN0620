import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity } from 'react-native';

export default class App extends React.Component {
    state = {
        password: ''
     }
    handlePassword = (text) => {
        this.setState({ password: text })
    }
  render(){
    return (
        <View style={styles.container}>
            <View style={styles.logo}>
                <Image source={require('./images/logo_putih.png')} style={{width:375, height:102}}/>
                <Text style={styles.logoText}>PORTOFOLIO</Text>
            </View>
            <View style={styles.loginTitle}>
                <Text style={styles.loginTitle}>Login</Text>
            </View>
            <View style={styles.inputView}>
                <TextInput  
                    style={styles.inputText}
                    placeholder="Email/Username" 
                    placeholderTextColor="#00d0ff"
                />
            </View>
            <View style={styles.inputView}>
                <TextInput  
                    style={styles.inputText}
                    placeholder="Password" 
                    placeholderTextColor="#00d0ff"
                    onChangeText = {this.handlePassword}
                    autoCapitalize = "none"
                />
            </View>
            <TouchableOpacity>
                <Text style={styles.forgot}>Forgot Password?</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.loginBtn}>
                <Text style={styles.loginText}>LOGIN</Text>
            </TouchableOpacity>
            <TouchableOpacity>
                <Text style={styles.signupText}>Signup</Text>
            </TouchableOpacity>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    marginBottom:-10  
  },
  logoText:{
    textAlign: 'right',
    fontSize:24,
    color:'#00d0ff',
    marginBottom:60,
    marginRight:28,
    marginTop:-35,
    fontFamily: 'Roboto'
  },
  loginTitle:{
    fontWeight:"bold",
    fontSize:30,
    fontFamily: 'Roboto',
    color:"#003366",
    textAlign:"center",
    marginBottom: 10
  },
  inputView:{
    width:"80%",
    backgroundColor:"#003366",
    borderRadius:25,
    height:50,
    marginBottom:20,
    justifyContent:"center",
    padding:20
  },
  inputText:{
    height:50,
    color:"white"
  },
  forgot:{
    color:"#003366",
    fontSize:11
  },
  loginBtn:{
    width:"30%",
    backgroundColor:"#fb5b5a",
    borderRadius:25,
    height:50,
    alignItems:"center",
    justifyContent:"center",
    marginTop:40,
    marginBottom:10
  },
  loginText:{
    fontWeight:"bold",
    fontSize:18,
    fontFamily: 'Roboto',
    color:"white",
    textAlign:"center",
  },
  signupText:{
    color:"#003366",
    fontSize:15
  },
});