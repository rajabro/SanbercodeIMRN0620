import React, {Component} from 'react';
import { StyleSheet, Text, View, Image, TextInput, TouchableOpacity, TouchableHighlight } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { AntDesign } from '@expo/vector-icons';
import { FontAwesome } from '@expo/vector-icons';

export default class App extends React.Component {
    state = {
        password: ''
     }
    handlePassword = (text) => {
        this.setState({ password: text })
    }
  render(){
    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <Text style={styles.title}>Tentang Saya</Text>
            </View>
            <TouchableHighlight>
                <View style={styles.profileImage}>
                    <Image source={require('./images/foto_profile.jpg')} style={[styles.profileImage, {width:200, height:200}]}/>
                </View>
            </TouchableHighlight>
            <View>
                <Text style={styles.profileName}>Ruba'i Amin Jaya</Text>
            </View>
            <View>
                <Text style={styles.profileDesc}>React Native Developer</Text>
            </View>
            <View style={styles.portofolioContainer}>
                <Text style={styles.portofolioText}>Portofolio</Text>
                <View style={styles.tabPortofolio} >
                  <TouchableOpacity>
                    <View style={styles.portofolioItem} >
                      <FontAwesome name="gitlab" size={30} color="#3EC6FF" />
                      <Text style={styles.profileDesc}>@rajabro</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.portofolioItem} >
                      <AntDesign name="github" size={30} color="#3EC6FF" />
                      <Text style={styles.profileDesc}>@rajabro</Text>
                    </View>
                  </TouchableOpacity>
                </View>
            </View>
            <View style={styles.contactContainer}>
                <Text style={styles.portofolioText}>Hubungi Saya</Text>
                <View style={styles.tabContact} >
                  <TouchableOpacity>
                    <View style={styles.contactItem} >
                      <FontAwesome name="facebook" size={30} color="#3EC6FF" />
                      <Text style={styles.profileDesc}>@rajabro</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.contactItem} >
                      <AntDesign name="instagram" size={30} color="#3EC6FF" />
                      <Text style={styles.profileDesc}>@rajabro</Text>
                    </View>
                  </TouchableOpacity>
                  <TouchableOpacity>
                    <View style={styles.contactItem} >
                      <AntDesign name="twitter" size={30} color="#3EC6FF" />
                      <Text style={styles.profileDesc}>@rajabro</Text>
                    </View>
                  </TouchableOpacity>
                </View>
            </View>
        </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  profileImage: {
    borderRadius: 100,
    borderColor: '#003366',
    borderWidth: 1,
    overflow: 'hidden'
  },
  profileName:{
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize:24,
    color:'#003366',
    fontFamily: 'Roboto'
  },
  profileDesc:{
    color:"#00bbff",
    fontSize:15,
    marginBottom: 10
  },
  title:{
    fontWeight:"bold",
    fontSize:30,
    fontFamily: 'Roboto',
    color:"#003366",
    textAlign:"center",
    marginBottom: 10
  },
  portofolioContainer:{
    width:"80%",
    backgroundColor:"#EFEFEF",
    borderRadius:10,
    height:100,
    marginBottom:20,
    padding: 5
  },
  portofolioText:{
    color:"#003366",
    fontSize:15,
    borderBottomColor : '#677699',
    borderBottomWidth: 1,
    textAlign: 'left',
    justifyContent:"flex-start",
  },
  contactContainer:{
    width:"80%",
    backgroundColor:"#EFEFEF",
    borderRadius:10,
    height:200,
    marginBottom:20,
    padding: 5
  },
  inputText:{
    height:50,
    color:"white"
  },
  forgot:{
    color:"#003366",
    fontSize:11
  },
  tabPortofolio: {
    height: 60,
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  portofolioItem: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 7,
  },
  tabContact: {
    height: 60,
    flexDirection: 'column',
    justifyContent: 'space-around',
    padding: 80
  },
  contactItem: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 15,
    marginHorizontal: 20,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});