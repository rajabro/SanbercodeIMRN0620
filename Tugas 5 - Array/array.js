console.log("Soal No 1 (Range)-------------------------");

function range(startNum, finishNum) {
    var rangeArray = []
    if (startNum == undefined || finishNum == undefined){
        return -1;
    } else if (startNum < finishNum) {
        for (i = startNum; i <= finishNum; i++){
            rangeArray.push(i);
        }
    } else {
        for (j = finishNum; j <= startNum; j++){
            rangeArray.push(j);
            rangeArray.sort(function (startNum, finishNum) {
                return finishNum - startNum;
            });
        }
    }
    return rangeArray;
}
console.log(range(1,10));
console.log(range(1));
console.log(range(11,18));
console.log(range(54,50));
console.log(range());

console.log()
console.log()
console.log("Soal No 2 (Range with step)-------------------------");

function rangeWithStep(startNum, finishNum, step) {
    var rangeArray = []
    if (startNum == undefined || finishNum == undefined){
        return -1;
    } else if (startNum < finishNum) {
        for (i = startNum; i <= finishNum; i += step){
            rangeArray.push(i);
        }
    } else {
        for (j = startNum; j >= finishNum; j -= step){
            rangeArray.push(j);
            rangeArray.sort(function (startNum, finishNum, step) {
                return finishNum - startNum;
            });
        }
    }
    return rangeArray;
}
console.log(rangeWithStep(1, 10, 2));
console.log(rangeWithStep(11, 23, 3));
console.log(rangeWithStep(5, 2, 1));
console.log(rangeWithStep(29, 2, 4));

console.log()
console.log()
console.log("Soal No 3 (Sum of Range)-------------------------");

function sum(num1, num2, step){
    var range = [];
    var jumlah = 0;
    if (num1 == undefined && num2 == undefined && step == undefined){
        return 0
    } else if (step == undefined && num1 < num2){
        for (k = num1; k <= num2; k++){
            range.push(k);
            jumlah += k
        }   
    } else if (step == undefined && num1 > num2) {
        for (k = num1; k >= num2; k--){
            range.push(k);
            jumlah += k
        }  
    } else if (num1 < num2){
        for (k = num1; k <= num2; k += step){
            range.push(k);
            jumlah += k
        }
    } else if (num1 > num2){
        for (k = num1; k >= num2; k -= step){
            range.push(k);
            jumlah += k
        }
    } else {
        range.push(num1);
        jumlah += num1
    }
    return jumlah;

}

console.log(sum(1,10));
console.log(sum(5, 50, 2));
console.log(sum(15,10));
console.log(sum(20, 10, 2));
console.log(sum(1));
console.log(sum());

console.log()
console.log()
console.log("Soal No. 4 (Array Multidimensi)----------------------------");

var data = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
]  

function dataHandling(input){
    var input = data.slice()
    jumlahData = input.length
    for (var i = 0; i < jumlahData; i++){
        console.log("Nomor ID :" + input[i][0])
        console.log("Nama Lengkap: " + input[i][1])
        console.log("TTL: " + input[i][2])
        console.log("Hobi: " + input[i][3])
    }
}

dataHandling();

console.log()
console.log()
console.log("Soal No. 5 (Balik Kata)--------------------------");

function balikKata(kata){
    var kataTerbalik = ""
    for (i = kata.length-1; i >=0; i--){
        kataTerbalik += kata.charAt(i)
    }
    return kataTerbalik
}

console.log(balikKata("Kasur Rusak")) 
console.log(balikKata("SanberCode")) 
console.log(balikKata("Haji Ijah")) 
console.log(balikKata("racecar")) 
console.log(balikKata("I am Sanbers")) 

console.log()
console.log()
console.log("Soal No. 6 (Metode Array)-------------------------")
var input = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"];
function dataHandling2(input){
    input2 = input.splice(1,1,"Roman Alamsyah Elsharawy");
    input3 = input.splice(2,1,"Provinsi Bandar Lampung");
    input4 = input.splice(4,1,"Pria");
    input5 = input.push("SMA Internasional Metro");
    console.log(input);
    var tanggal = input.slice(3,4);
    var stringTanggal = tanggal.toString();
    var bulan = stringTanggal.substr(3,2)
    switch (bulan){
        case "01": {console.log("Januari");break; }
        case "02": {console.log("Februari");break; }
        case "03": {console.log("Maret");break; }
        case "04": {console.log("April");break; }
        case "05": {console.log("Mei");break; }
        case "06": {console.log("Juni");break; }
        case "07": {console.log("Juli");break; }
        case "08": {console.log("Agustus");break; }
        case "09": {console.log("September");break; }
        case "10": {console.log("Oktober");break; }
        case "11": {console.log("November");break; }
        case "12": {console.log("Desember");break; }
        default: {console.log("Bulan tidak valid");break; }
    }

}
dataHandling2(input);

var tanggal = input.slice(3)