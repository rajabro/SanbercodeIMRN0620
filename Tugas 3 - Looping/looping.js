console.log("LOOPING PERTAMA");
var angka = 2;

while (angka<=20) {
    console.log(angka + " - I Love coding");
    angka += 2;
}

console.log("LOOPING KEDUA");
var angka = 20;

while (angka > 0) {
    console.log(angka + " - I will become a mobile developer");
    angka -= 2;
}

console.log("LOOPING MENGGUNAKAN FOR")

for (var angka = 1; angka <= 20; angka++) {
    if (angka % 2 == 0) {
        console.log(angka + " - Berkualitas");
    } else if (angka % 2 != 0 && angka % 3 == 0){
        console.log(angka + " - I Love Coding");
    } else {
        console.log(angka + " - Santai");
    }
}

console.log("MEMBUAT PERSEGI PANJANG");

var lebar = 4;
var panjang = 8;
var kotak = "";
for(var j = 1; j <=lebar; j++){
    for (var i = 1; i <= panjang; i++) {
        kotak += "#";
    }
    kotak += "\n";
}
console.log(kotak);

console.log("MEMBUAT TANGGA");

var tinggi = 7;
var kotak = "";
for(var j = 1; j <=tinggi; j++){
    for (var i = 1; i <= j; i++) {
        kotak += "#";
    }
    kotak += "\n";
}
console.log(kotak);

console.log("MEMBUAT PAPAN CATUR");

var lebar = 8;
var panjang = 8;
var papan = "";

for (l = 1; l <= lebar; l++){
    if (l % 2 == 0){
        for (k = 1; k <= panjang ; k++){
            if (k % 2 == 0){
                papan += " ";
            } else {
                papan += "#";
            }
        }
    } else {
        for (k = 1; k <= panjang ; k++){
            if (k % 2 == 0){
                papan += "#";
            } else {
                papan += " ";
            }
        }
    }
    papan += "\n"
}
console.log(papan)
