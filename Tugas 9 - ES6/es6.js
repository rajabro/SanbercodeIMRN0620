// const golden = function goldenFunction(){
//     console.log("this is golden!!")
//   }
   
//   golden()

const golden = (goldenFunction) => {
    console.log("this is golden!!");
}

golden()


const newFunction = (firstName, lastName) => {
    let fullName= `${firstName} ${lastName}`
    console.log(fullName);
  }
   
  //Driver Code 
newFunction("William", "Imoh");


const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  };

const { firstName, lastName, destination, occupation, spell } = newObject;

  // Driver code
console.log(firstName, lastName, destination, occupation);

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

const planet = "earth"
const view = "glass"
var before = `Lorem ${view} dolor sit amet,
    consectetur adipiscing elit, ${planet} do eiusmod tempor
    incididunt ut labore et dolore magna aliqua. Ut enim 
    ad minim veniam`
 
// Driver Code
console.log(before) 