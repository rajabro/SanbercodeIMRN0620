//tugas if- else

var nama = "Junaedi";
var peran = "Werewolf";
var peranPenyihir = "kamu dapat melihat siapa yang menjadi werewolf!";
var peranGuard = "kamu akan membantu melindungi temanmu dari serangan werewolf.";
var peranWerewolf = "kamu akan memakan mangsa setiap malam!";


if (nama == '' && peran == '') {
    console.log("Nama harus diisi")
} else if (nama != '' && peran == '') {
    console.log("Halo " + nama + ", Pilih peranmu untuk memulai game")   
} else if (nama != '' && peran == 'Penyihir') {
    console.log("Selamat Datang di Dunia Werewolf, " + nama)
    console.log("Halo " + peran + " " + nama + ", " + peranPenyihir)
} else if (nama != '' && peran == 'Guard') {
    console.log("Selamat Datang di Dunia Werewolf, " + nama)
    console.log("Halo " + peran + " " + nama + ", " + peranGuard)
} else if (nama != '' && peran == 'Werewolf') {
    console.log("Selamat Datang di Dunia Werewolf, " + nama)
    console.log("Halo " + peran + " " + nama + ", " + peranWerewolf)
}

//tugas switch
var tanggal = 12
var bulan = 7
var tahun = 2020

switch (true) {
    case (tanggal <1 || tanggal >31): {
        console.log("Input tanggal salah")
        break;
    }
    case (bulan <1 || bulan > 12): {
        console.log("Input bulan salah")
        break;
    }
    case (tahun < 1900 || tahun > 2200): {
        console.log("Input tahun salah")
        break;
    }
    default: {
        switch (bulan) {
            case 1: {console.log(tanggal + " " + "Januari" + " " + tahun);break}
            case 2: {console.log(tanggal + " " + "Februari" + " " + tahun);break}
            case 3: {console.log(tanggal + " " + "Maret" + " " + tahun);break}
            case 4: {console.log(tanggal + " " + "April" + " " + tahun);break}
            case 5: {console.log(tanggal + " " + "Mei" + " " + tahun);break}
            case 6: {console.log(tanggal + " " + "Juni" + " " + tahun);break}
            case 7: {console.log(tanggal + " " + "Juli" + " " + tahun);break}
            case 8: {console.log(tanggal + " " + "Agustus" + " " + tahun);break}
            case 9: {console.log(tanggal + " " + "September" + " " + tahun);break}
            case 10: {console.log(tanggal + " " + "Oktober" + " " + tahun);break}
            case 11: {console.log(tanggal + " " + "November" + " " + tahun);break}
            case 12: {console.log(tanggal + " " + "Desember" + " " + tahun);break}
            default: {console.log("Data tidak valid");break}
        }
    }
        break;
}
