class Animal{
    constructor(name, legs, cold_blooded){
        this.name = name;
        this.legs = 4;
        this.cold_blooded = false
    }

    get cname(){
        return this.name;
    }
    set cname(x){
        this.name = x;
    }
}

var sheep = new Animal("shaun");

console.log(sheep.name) // "shaun"
console.log(sheep.legs) // 4
console.log(sheep.cold_blooded) // false

class Ape extends Animal{
    constructor(name, legs, cold_blooded){
        super(name, cold_blooded);
        this.legs = 2;
    }
    
    yell(){
        console.log("Auooo");
    }
}

var sungokong = new Ape("kera sakti")
console.log(sungokong.name)
sungokong.yell() // "Auooo"


class Frog extends Animal{
    constructor(name, legs, cold_blooded){
        super(name, legs, cold_blooded);
        this.cold_blooded = true;
    }
    
    jump(){
        console.log("hop hop");
    }
}

var kodok = new Frog("buduk")
console.log(kodok.name)
kodok.jump() // "hop hop"


/*
function Clock({ template }) {
  
    var timer;
  
    function render() {
      var date = new Date();
  
      var hours = date.getHours();
      if (hours < 10) hours = '0' + hours;
  
      var mins = date.getMinutes();
      if (mins < 10) mins = '0' + mins;
  
      var secs = date.getSeconds();
      if (secs < 10) secs = '0' + secs;
  
      var output = template
        .replace('h', hours)
        .replace('m', mins)
        .replace('s', secs);
  
      console.log(output);
    }
  
    this.stop = function() {
      clearInterval(timer);
    };
  
    this.start = function() {
      render();
      timer = setInterval(render, 1000);
    };
  
  }
  
  var clock = new Clock({template: 'h:m:s'});
  clock.start();
  */
 
  class Clock {
    constructor({template}){
      this.template = template 
    }
    render() {
        this.timer;
        var date = new Date();
    
        var hours = date.getHours();
        if (hours < 10) hours = '0' + hours;
    
        var mins = date.getMinutes();
        if (mins < 10) mins = '0' + mins;
    
        var secs = date.getSeconds();
        if (secs < 10) secs = '0' + secs;
    
        var output = this.template
          .replace('h', hours)
          .replace('m', mins)
          .replace('s', secs)
    
        console.log(output);
      }

    stop() {
        clearInterval(this.timer);
     };
    
    start() {
      this.timer = setInterval(this.render.bind(this), 1000);
    };

}

var clock = new Clock({template: 'h:m:s'});
clock.start();