
var arr = [
    ["Abduh", "Muhammad", "male", 1992],
    ["Ahmad", "Taufik", "male", 1985]
]
var people = [ ["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"] ]
var people2 = [ ["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023] ]

var now = new Date();
var thisYear = now.getFullYear();
var ageInt = ""
var arrObj = {}

function arrayToObject(arr){
    for(i = 0; i < arr.length; i++){
        if(arr[i][3] > thisYear || arr[i][3] == undefined){
            ageInt = "Invalid Birth Year"
        } else{
            ageInt = thisYear - arr[i][3];
        }
        var objArr = {}
        objArr.firsname = arr[i][0];
        objArr.lastname = arr[i][1];
        objArr.gender = arr [i][2];
        objArr.age = ageInt;

        console.log(i+1 + ". " + objArr.firsname + objArr.lastname + ":")
        console.log(objArr)
    }
}


/*
    1. Tony Stark: { 
        firstName: "Tony",
        lastName: "Stark",
        gender: "male",
        age: 40
    }
    2. Pepper Pots: { 
        firstName: "Pepper",
        lastName: "Pots",
        gender: "female".
        age: "Invalid Birth Year"
    }
*/

arrayToObject(arr); 
arrayToObject(people); 
arrayToObject(people2); 

var belanja = []
var shoppingObj = {}

function shoppingTime(memberId, money) {
    // you can only write your code here!
    
    if(memberId == undefined || memberId == ""){
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else if (money < 50000){
        return "Mohon maaf, uang tidak cukup"
    } else {
        var changeMoney = money
        if(changeMoney >= 1500000){
            belanja.push("Sepatu brand Stacattu")
            changeMoney -= 1500000
        }
        if(changeMoney >= 500000){
            belanja.push("Baju brand Zoro")
            changeMoney -= 500000
        }
        if(changeMoney >= 250000){
            belanja.push("Baju brand H&N")
            changeMoney -= 250000
        }
        if(changeMoney >= 175000){
            belanja.push("Sweater brand Uniklooh")
            changeMoney -= 175000
        }
        if(changeMoney >= 50000){
            belanja.push("Casing Handphone")
            changeMoney -= 50000
        }
        shoppingObj.memberId = memberId;
        shoppingObj.money = money;
        shoppingObj.listPurchased= belanja;
        shoppingObj.changeMoney= changeMoney;
        belanja=[]
        changeMoney=0
        return shoppingObj;
    }
  }
   
  // TEST CASES
  console.log(shoppingTime('1820RzKrnWn08', 2475000));
    //{ memberId: '1820RzKrnWn08',
    // money: 2475000,
    // listPurchased:
    //  [ 'Sepatu Stacattu',
    //    'Baju Zoro',
    //    'Baju H&N',
    //    'Sweater Uniklooh',
    //    'Casing Handphone' ],
    // changeMoney: 0 }
  console.log(shoppingTime('82Ku8Ma742', 170000));
  //{ memberId: '82Ku8Ma742',
  // money: 170000,
  // listPurchased:
  //  [ 'Casing Handphone' ],
  // changeMoney: 120000 }
  console.log(shoppingTime('', 2475000)); //Mohon maaf, toko X hanya berlaku untuk member saja
  console.log(shoppingTime('234JdhweRxa53', 15000)); //Mohon maaf, uang tidak cukup
  console.log(shoppingTime()); ////Mohon maaf, toko X hanya berlaku untuk member saja

  function naikAngkot(arrPenumpang) {
    rute = ['A', 'B', 'C', 'D', 'E', 'F'];
    var total = []
    for (i=0; i < arrPenumpang.length; i++){
        var penumpang = arrPenumpang[i][0];
        var naikDari = arrPenumpang[i][1];
        var tujuan = arrPenumpang[i][2];
        var nilaiNaik = Number(rute.indexOf(naikDari))
        var nilaiTujuan = Number(rute.indexOf(tujuan))
        var nilaiBayar = (nilaiTujuan - nilaiNaik) * 2000
        var bayar = nilaiBayar
        
        dataPenumpang = {}
        dataPenumpang.penumpang = penumpang
        dataPenumpang.naikDari = naikDari
        dataPenumpang.tujuan = tujuan
        dataPenumpang.bayar = bayar
        total.push(dataPenumpang)
    }
    return total
  }
   
  //TEST CASE
  console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
  // [ { penumpang: 'Dimitri', naikDari: 'B', tujuan: 'F', bayar: 8000 },
  //   { penumpang: 'Icha', naikDari: 'A', tujuan: 'B', bayar: 2000 } ]
   
  console.log(naikAngkot([])); //[]

